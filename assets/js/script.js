// A saddle point is an element of the matrix such that it is the minimum element in its row and maximum in its column.
// steps
// 1. generate matrix
// 2. call function that will generate the matrix
// 3. get the minimum in a row.
// 4. call function in step 3 to determine the minimum in each row.
// 5. get the maximum in each column.
// 6. call function in step 5 to determine the maximum in each column.
// 7. compare the corresponding minimum and maximum in each column and see if at least one matches (if matched, saddle point!). 
    // If no match, regenerate matrix until a saddle point is found.

/**************************************************************************************/
// prob1: generate matrix 3x3
// prob2: per line/array get minimum
// > subprob: put per line/array in a tempArray
//  > subprob: getMin
//	>> subsubprob: store index of Min in each array.
//  > subprob: store in temp array all Mins
//  > store index of Min in each array.


  /*global variables*/
  var initialGame = 0;
  var matrix;
  var storeIndex = 0;
  var tempStoreMinMaxValsArr = [];
  var x = 0;  //the col or row number in the matrix.
  var countB4SaddleMat = 0;
  var saddleVal = {};
  var size = 3;
  var level = 0;
  var conditions = false;
  var saddlePoint;
  var gamerPoints = 0;
  let status = document.querySelector(`#status`);
  let points = document.querySelector(`#points`);
  let pointsNum = document.querySelector(`#pointsNum`);

function reinitializeVariables1(){
  /*global variables*/
  matrix;
  storeIndex = 0;
  tempStoreMinMaxValsArr = [];
  x = 0;
  countB4SaddleMat = 0;
  saddleVal = {};
  size = prompt("Enter matrix size. Should be between 3-8 only.");
  level = parseInt(prompt("Enter level of difficulty. Levels are 1 - 5"));
  conditions = false;
  saddlePoint;
}

function reinitializeVariables2(){
  /*global variables*/
  matrix;
  storeIndex = 0;
  tempStoreMinMaxValsArr = [];
  x = 0;
  countB4SaddleMat = 0;
  saddleVal = {};
  size = parseInt(size);
  level = parseInt(level) + 1;
  //conditions = false;
  saddlePoint;
}

function initializeGame(){
  console.log('conditions: ' + conditions);
  console.log('matrix size: ' + size);
  console.log('matrix level: ' + level);
  while (conditions == false) {
    if(parseInt(size) >=3 && parseInt(size) <=8 ){
      if(parseInt(level)>=1 && parseInt(level)<=5 ){
        conditons = true;     //if level and size are within the range, then conditions becomes 'true', thus can go out of the loop after generating matrix based on the level and size provided by user.
        console.log("Size: " + size);
        console.log("Level: " + level);
        let gameLevel = document.querySelector(`#gameLevel`);
        gameLevel.textContent = `Difficulty level: ${level}`;
        let gameMatrixSize = document.querySelector(`#gameMatrixSize`);
        gameMatrixSize.textContent = `Matrix size: ${size}`;
        saddlePoint = getSaddlePoint(size, level);
        console.log("Saddle POINT: " + saddlePoint.value);
        console.log("Saddle ROW: " + saddlePoint.row);
        console.log("Saddle COL: " + saddlePoint.col);
        console.log("***************Gamer clicks are below*****************");
        break;
      } else {
        if(initialGame == 0) {
          console.log("level should be between 1 - 5 only");
          level = prompt("Enter level of difficulty. Levels are 1 - 5");
        } else {
          level = 1;  //reinitialize the level to 1
          size = parseInt(size) + 1;  //then, have the next size of matrix
        }
      }
    } else {
      //console.log("size: " + size);
      //console.log("test else");
      console.log("size should be between 3 -8 only");
      size = prompt("Enter matrix size. Should be etween 3-8 only.");
    }
  }
}

// MAIN FUNCTION | getSaddlePoint(size)
function getSaddlePoint(size, level){
  //let level = level;
  let isSaddle = false;
  let matrix;
  let minValsObj;
  let maxValsObj;

  while(isSaddle === false){

    matrix = genMatrix(size, level);
    minValsObj = callMinFunc(matrix, size);
    //console.log("************END OF MIN************************");
    storeIndex = 0;
    tempStoreMinMaxValsArr = [];
    x = 0;
    maxValsObj = callMaxFunc(matrix, size);

    for(let y=0; y<=size-1; y++){
      for(let i=0; i<=size-1; i++){
        if(minValsObj[y].minVal == maxValsObj[i].maxVal  &&  minValsObj[y].col == maxValsObj[i].col  &&  minValsObj[y].row == maxValsObj[i].row){
                console.log("Game level is " + level);
                console.log("Matrix size is " + size);
                console.log("Original Matrix");
                console.log(matrix);
                console.log("MINs values");
                console.log(minValsObj);
                console.log("MAX's values");
                console.log(maxValsObj);
                console.log("MIN point: " + minValsObj[y].minVal);
                console.log("col: " + minValsObj[y].col);
                console.log("row: " + minValsObj[y].row);
                console.log("MAX point: " + maxValsObj[i].maxVal);
                console.log("col: " + maxValsObj[i].col);
                console.log("row: " + maxValsObj[i].row);
                console.log("Therefore, SADDLE POINT : " + minValsObj[y].minVal);
                console.log("and Saddle Point COL : " + minValsObj[y].col);
                console.log("and finally Saddle Point ROW : " + minValsObj[y].row);

                console.log("No. of auto-generated matrix before finding a matrix with saddle point: " + countB4SaddleMat);
                isSaddle = true;      // if the matrix generated has a saddle point then make 'saddle' be 'true' to stop generating matrix
                saddleVal = { value: minValsObj[y].minVal,     // Saddle value
                              col: minValsObj[y].col,
                              row: minValsObj[y].row };
          //}
        }
      }
    }

    if(isSaddle === true){  // if true then a matrix with saddle point has been generated
      reflectNumInBoxes();
      return saddleVal;     // thus, go out of for loop
    }
    storeIndex = 0;
    tempStoreMinMaxValsArr = [];
    x = 0;
    countB4SaddleMat++;
  }
}

/*1. Generate matrix*/
function genMatrix(size) {
  let i = 0;
  matrix = new Array(size);   //declare length of array (no. of rows)
  while(i <= size-1){
    let y = 0;
    matrix[i] = new Array(size);    //declare length of subarray (no. of columns)
    while(y <= size-1){
      if(level == 1 || level == '1'){
        matrix[i][y] = Math.floor(Math.random() * 55);
      } else if (level == 2 || level == '2') {
        matrix[i][y] = Math.floor(Math.random() * (135 - 60)) + 60; //* (max - min)) + min;
      } else if (level == 3 || level == '3') {
        matrix[i][y] = Math.floor(Math.random() *(310 - 185)) + 185; //250
      } else if (level == 4 || level == '4') {
        matrix[i][y] = Math.floor(Math.random() * (550 - 390)) + 390; //550
      } else { //5
        matrix[i][y] = Math.floor(Math.random() * (1100 - 950)) + 950;  ////* (max - min)) + min;
      }
      y++;
    }
    i++;
  }
  return matrix;
}


/*2. Main Function to get MIN in each ROW */
function callMinFunc(matrix, size){
  let minVals;
  for(let i=0; i<=size-1; i++) {
    //store all min values in each ROW in a temp array
    minVals = storeMinMaxVals(matrix, "min");
  }
  return minVals;
}


/*3. Main Function to get MAXIMUM in each COLUMN */
function callMaxFunc(matrix, size){
  let transMatrix = transpose(matrix);  //transpose matrix first
  let maxVals;
  for(let i=0; i<=size-1; i++) {
    //store all max's values in each COLUMN in a temp array
    maxVals = storeMinMaxVals(transMatrix, "max");
  }
  return maxVals;
}


/*3. subprob: store in temp array all Mins (solution) */
function storeMinMaxVals(matrix, minOrMax){
  if(minOrMax === "min"){
    //store in temp array each ROW of the matrix to before getting the MIN of each row.
    tempStoreMinMaxValsArr[storeIndex] = findMin(getMatrixEachRow(matrix)); 
  } else {
    //store in temp array each 'row' of the matrix to before getting the MAX of each 'row' (COLUMN actually). 
    //(max matrix was transposed so column temporarily is the row).
    tempStoreMinMaxValsArr[storeIndex] = findMax(getMatrixEachRow(matrix));
  }
  storeIndex++;
  return tempStoreMinMaxValsArr;
}


/*4. subprob: getMin (solution) */
function findMin(arr){
let min = arr[0];
let temp_i = 0;
    for (i=0; i<=arr.length; i++){
        if (arr[i]<min){
              min = arr[i];
              temp_i = i;
        }else {
            min = min;
        }
    }
    let minObj = {
                  minVal: min,
                  row: x-1,
                  col: temp_i
                }
    return minObj;
}

/*5. subprob: getMax (solution) */
function findMax(arr){
  let max = arr[0];
  let temp_i = 0;
      for (i=0; i<=arr.length; i++){
          if (arr[i]>max){
            max = arr[i];
                temp_i = i;
          }else {
            max = max;
          }
      }
      let maxObj = {
                    maxVal: max,
                    row: temp_i, //row is the reverse of variable at 'row MIN', and as well as col
                    col: x-1     //this is because on getting max, matrix was transposed
                  }
      return maxObj;
      //return max;
  }

/*6. Used for max, transpose first the matrix before getting each row */
function transpose(a) {
  // Calculate the width and height of the Array
  let w = a.length || 0;
  let h = a[0] instanceof Array ? a[0].length : 0;

  // In case it is a zero matrix, no transpose routine needed.
  if (h === 0 || w === 0) { return []; }

  /**
   * @let {Number} i Counter
   * @let {Number} j Counter
   * @let {Array} t Transposed data is stored in this array.
   */
  let i, j, t = [];

  // Loop through every item in the outer array (height)
  for (i = 0; i < h; i++) {

      // Insert a new row (array)
      t[i] = [];

      // Loop through every item per item in outer array (width)
      for (j = 0; j < w; j++) {

          // Save transposed data.
          t[i][j] = a[j][i];
      }
  }
  return t;
}


/*7. get every row of the matrix */
//let x = 0;
function getMatrixEachRow(matrix){
  let tempRow = [];
  for(let i=0; i<matrix.length; i++){
    tempRow[i] = matrix[x][i];
  }
  x++;
  return tempRow;
}
//console.log(getMatrixEachRow(matrix));



/*=============================================================================================*/
//======================================= create dynamic boxes
function createBoxes(size){
  const parentContainer = document.querySelector(`#box-container`);
  for(let i=0; i<size; i++){
      //create row
      const row = document.createElement("div");
      row.setAttribute("class", 'row');      //put attribute 'class' on the element div; name the class 'container'.
      row.setAttribute("id", `row_${i}`);

      for(let y=0; y<size; y++){
        //create col
        const col = document.createElement("column");
        col.setAttribute("class", 'box');
        col.setAttribute("id", `col-box_${i}_${y}`);

        //append col in id=row_${i}
        row.appendChild(col);
      }
  //append col in id=row_${i}
  parentContainer.appendChild(row); 
  }
}


//======================================= remove boxes
function removeBoxes(){
  const parentContainer = document.querySelector(`#box-container`);
  let row;

  for(let i=0; i<size; i++){
    row = document.querySelector(`#row_${i}`);
    parentContainer.removeChild(row); 
  }
}


/*=================reflect to boxes the matrix numbers===============================*/
function reflectNumInBoxes() {
  createBoxes(size)
  const boxContainer = document.querySelector(`#box-container`);
  
  let box = document.querySelector(`#col-box_0_0`);
  box.textContent = matrix[0][0];
  for(let i=0; i<size; i++){
    for(let y=0; y<size; y++){
      box = document.querySelector(`#col-box_${i}_${y}`);
      box.textContent = matrix[i][y];
    }
  }
}
//reflectNumInBoxes();



//Event listener function
/* function(e) maglalabas ng event kahit saang part ka ng webpage magclick*/
document.addEventListener("click", function(e){
  //console.log(e);
  let gamerBoxClick = e.target.id;    //get the html id of the box where the user clicked
    //if gamer has clicked boxes inside the matrix [gamerBoxClick.slice(0,7)], then evaluate the box clicked by gamer
    if(gamerBoxClick.slice(0,7) == 'col-box'){

      let gamerNumAns = e.target.innerText;
      console.log("User clicked box:  " + gamerBoxClick);
      console.log("Box clicked has value:  " + gamerNumAns);

      correctBox = `col-box_${saddlePoint.row}_${saddlePoint.col}`;
      console.log("The box with saddle point:  " + correctBox);
      console.log("The value of saddle point:  " + saddlePoint.value);
            
      //see if the answer of gamer is correct.      
      if( gamerNumAns == saddlePoint.value && gamerBoxClick == `col-box_${saddlePoint.row}_${saddlePoint.col}`) {
        initialGame = 1;  //means that user has finished one round and therefore succeeding size and levels will free-flow and not be ased again and again (unless user clicks again 'New Game')
        getPoints(size, level);
        console.log(`Correct! You've now got ${gamerPoints} point(s)!`);

        let status = document.querySelector(`#status`);
        console.log(status.textContent);
        status.textContent = `Correct!`;

        pointsNum.textContent = gamerPoints;
        console.log("gamerPoints");
        console.log(gamerPoints);
        console.log("pointsNum");
        console.log(pointsNum);
        console.log(pointsNum.textContent);
        
        if(parseInt(size) == 8 && parseInt(level) == 5){
          status.textContent = `CONGRATULATIONS! You saddled the game!`;
        }

        console.log("******************************************************************");
        setTimeout(newMatrixSet, 1500); //generate new matrix (with 1 second delay);
      } else {
        if(gamerPoints > 0){
          status.textContent = `Nope! You have been deducted 1 point. Please try again.`;
          console.log(status.textContent);
          console.log("You have been deducted 1 point.");
          gamerPoints--;
        } else {
          status.textContent =`Nope! Try again.`;
        }

        pointsNum.textContent = gamerPoints;
        console.log(`Nope! Your current point is ${gamerPoints}. Please Try again!`);
      }
    }
  }
)

// function validateGamerAns(){
// }

function getPoints(size, level){
  switch(parseInt(size)){
    case 3:
      switch(parseInt(level)){
      case 1: gamerPoints = gamerPoints + 1; break;
      case 2: gamerPoints = gamerPoints + 2; break;
      case 3: gamerPoints = gamerPoints + 3; break;
      case 4: gamerPoints = gamerPoints + 4; break;
      case 5: gamerPoints = gamerPoints + 5; break;
      default: gamerPoints = gamerPoints + 0; break;
    } break;
    case 4:
      switch(parseInt(level)){
      case 1: gamerPoints = gamerPoints + 2; break;
      case 2: gamerPoints = gamerPoints + 4; break;
      case 3: gamerPoints = gamerPoints + 6; break;
      case 4: gamerPoints = gamerPoints + 8; break;
      case 5: gamerPoints = gamerPoints + 10; break;
      default: gamerPoints = gamerPoints + 0; break;
    } break;
    case 5:
      switch(parseInt(level)){
      case 1: gamerPoints = gamerPoints + 3; break;
      case 2: gamerPoints = gamerPoints + 6; break;
      case 3: gamerPoints = gamerPoints + 9; break;
      case 4: gamerPoints = gamerPoints + 12; break;
      case 5: gamerPoints = gamerPoints + 15; break;
      default: gamerPoints = gamerPoints + 0; break;
    } break;
    case 6:
      switch(parseInt(level)){
      case 1: gamerPoints = gamerPoints + 4; break;
      case 2: gamerPoints = gamerPoints + 8; break;
      case 3: gamerPoints = gamerPoints + 12; break;
      case 4: gamerPoints = gamerPoints + 16; break;
      case 5: gamerPoints = gamerPoints + 20; break;
      default: gamerPoints = gamerPoints + 0; break;
    } break;
    case 7:
      switch(parseInt(level)){
      case 1: gamerPoints = gamerPoints + 5; break;
      case 2: gamerPoints = gamerPoints + 10; break;
      case 3: gamerPoints = gamerPoints + 15; break;
      case 4: gamerPoints = gamerPoints + 20; break;
      case 5: gamerPoints = gamerPoints + 25; break;
      default: gamerPoints = gamerPoints + 0; break;
    } break;
    case 8:
      switch(parseInt(level)){
      case 1: gamerPoints = gamerPoints + 6; break;
      case 2: gamerPoints = gamerPoints + 12; break;
      case 3: gamerPoints = gamerPoints + 18; break;
      case 4: gamerPoints = gamerPoints + 24; break;
      case 5: gamerPoints = gamerPoints + 32; break;
      default: gamerPoints = gamerPoints + 0; break;
    } break;
    default: break;
  }
}


function newMatrixSet(){
//new matrix will generate after the user has selected the correct saddle point.
    removeBoxes();
    reinitializeVariables2();
    initializeGame();
}


let newGame = document.querySelector("#new_game");
newGame.addEventListener("click", function(e){
  removeBoxes();
  reinitializeVariables1();
  initializeGame();
})

